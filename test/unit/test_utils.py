from __future__ import annotations


def test_build_bom_dict_from_purls():
    ...


def test_build_bom_from_purls():
    ...


def test_convert_xml_to_json():
    ...


def test_create_sbom_object():
    ...


def test_get_advisories_from_urls():
    ...


def test_get_references_from_ids():
    ...


def test_get_vulnerability_source():
    ...


def test_parse_sbom():
    ...


def test_parse_sbom_json_string():
    ...

# Hoppr Security Commons 

This module provides common reporting,  a [pluggable interface](security_commons/common/vulnerability_scanner.py) for vulnerabilities and various utility functions.  

It is intended for integrating with [hoppr-cop](https://gitlab.com/lmco/hoppr/utilities/supply-chain-security/hoppr-cop) or hoppr plugins wishing to provide similar reporting functionality.

### Installation 

## Pip 
`pip install hoppr-security-commons --extra-index-url https://gitlab.com/api/v4/projects/38610490/packages/pypi/simple`
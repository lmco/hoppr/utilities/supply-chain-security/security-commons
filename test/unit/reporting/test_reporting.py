from __future__ import annotations


def test_reporting__add_purl_as_bom_ref_and_flatten():
    ...


def test_reporting__copy_assets():
    ...


def test_reporting__generate_gitlab_row():
    ...


def test_reporting__generate_html_report():
    ...


def test_reporting__generate_vuln_detail_reports():
    ...


def test_reporting__get_best_rating():
    ...


def test_reporting__get_featured_link():
    ...


def test_reporting__get_fields_from_vulnerabilities():
    ...


def test_reporting__get_severity():
    ...


def test_reporting_add_vulnerabilities_to_bom():
    ...


def test_reporting_generate_gitlab_vulnerability_report():
    ...


def test_reporting_generate_vulnerability_reports():
    ...


def test_reporting_get_score():
    ...


def test_reporting_link_vulnerabilities_to_bom():
    ...

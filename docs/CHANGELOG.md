## [0.0.114](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.113...v0.0.114) (2024-11-19)


### Bug Fixes

* **deps:** update dependency typer to v0.13.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([daaf28f](https://gitlab.com/hoppr/modules/security-commons/commit/daaf28fd678075477c07eb9fcb9d8a159097a8e2))

## [0.0.113](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.112...v0.0.113) (2024-11-15)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.15 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0fe34f6](https://gitlab.com/hoppr/modules/security-commons/commit/0fe34f6510a88f64e26adde827f9af0a75713e29))

## [0.0.112](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.111...v0.0.112) (2024-11-08)


### Bug Fixes

* **deps:** update dependency typer to v0.13.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9bb4e5f](https://gitlab.com/hoppr/modules/security-commons/commit/9bb4e5f892a5a0bed11ce5f81e322d7af8e84bd7))

## [0.0.111](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.110...v0.0.111) (2024-10-22)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.16.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([09e29d0](https://gitlab.com/hoppr/modules/security-commons/commit/09e29d0814db3f15bd2603e42feb19a41379d7d9))

## [0.0.110](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.109...v0.0.110) (2024-10-15)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.14 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d4acb13](https://gitlab.com/hoppr/modules/security-commons/commit/d4acb13ccde01273c095fc1dc3372c1760844f54))

## [0.0.109](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.108...v0.0.109) (2024-09-23)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.13 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8fb866c](https://gitlab.com/hoppr/modules/security-commons/commit/8fb866c75d54afd3b161f08aefe911fc452c4e02))

## [0.0.108](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.107...v0.0.108) (2024-09-17)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.12 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2697b7e](https://gitlab.com/hoppr/modules/security-commons/commit/2697b7ede3b00f62d8938c1a7eb1c2cead915dab))

## [0.0.107](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.106...v0.0.107) (2024-09-11)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.11 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0c08c97](https://gitlab.com/hoppr/modules/security-commons/commit/0c08c97560d911cd487da4db8ced93e21e013fe6))

## [0.0.106](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.105...v0.0.106) (2024-09-05)


### Bug Fixes

* **deps:** update dependency typer to v0.12.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([26886ba](https://gitlab.com/hoppr/modules/security-commons/commit/26886ba3ba04dfd89e07a1ee80cc6ead41534053))

## [0.0.105](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.104...v0.0.105) (2024-08-17)


### Bug Fixes

* **deps:** update dependency typer to v0.12.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([04bbbe9](https://gitlab.com/hoppr/modules/security-commons/commit/04bbbe949069ee47b3e077136031e587bcab7097))

## [0.0.104](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.103...v0.0.104) (2024-08-13)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.10 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e9959f8](https://gitlab.com/hoppr/modules/security-commons/commit/e9959f81aaa525e8eaa06d0fb74654983acd879a))

## [0.0.103](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.102...v0.0.103) (2024-08-08)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.9 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b00ab27](https://gitlab.com/hoppr/modules/security-commons/commit/b00ab27125bb74e102d75c328b25e18ca5548c22))

## [0.0.102](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.101...v0.0.102) (2024-08-07)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.8 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9a63b78](https://gitlab.com/hoppr/modules/security-commons/commit/9a63b78871a48e7266bf8d2ceb3445e82e482c54))

## [0.0.101](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.100...v0.0.101) (2024-07-25)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.15.6 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([599cd00](https://gitlab.com/hoppr/modules/security-commons/commit/599cd00b5859e3fe9eda98aefc5a6df01f0ada9e))
* **deps:** update dependency tailwindcss to v3.4.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5f457be](https://gitlab.com/hoppr/modules/security-commons/commit/5f457be323d0915bf371a192dffc019e8020e450))

## [0.0.100](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.99...v0.0.100) (2024-07-24)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.15.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([fe69e09](https://gitlab.com/hoppr/modules/security-commons/commit/fe69e0975a59129e208e235b9b6a81986f0113e1))

## [0.0.99](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.98...v0.0.99) (2024-07-16)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.6.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([16302a3](https://gitlab.com/hoppr/modules/security-commons/commit/16302a3c65ad822e41e46b5ac76e8eb60c07ce41))

## [0.0.98](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.97...v0.0.98) (2024-07-16)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.6 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d4d0e5b](https://gitlab.com/hoppr/modules/security-commons/commit/d4d0e5b55d44d66585fb93b914908583af8044f0))

## [0.0.97](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.96...v0.0.97) (2024-07-15)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([88eb148](https://gitlab.com/hoppr/modules/security-commons/commit/88eb1486edf0fafc9a0a10a3a6a9830dc943145d))

## [0.0.96](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.95...v0.0.96) (2024-07-15)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.15.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([24ab2d2](https://gitlab.com/hoppr/modules/security-commons/commit/24ab2d21b43f81aa8bd4fa55f77469bbfa23d02e))

## [0.0.95](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.94...v0.0.95) (2024-07-09)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.15.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9d060d4](https://gitlab.com/hoppr/modules/security-commons/commit/9d060d4b31c3731900a796914d5655e563db3674))

## [0.0.94](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.93...v0.0.94) (2024-07-04)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.15.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9649ac9](https://gitlab.com/hoppr/modules/security-commons/commit/9649ac93281fe70ac9b6509ae75c32f33bd7e99f))

## [0.0.93](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.92...v0.0.93) (2024-06-20)

## [0.0.92](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.91...v0.0.92) (2024-06-13)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.15.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([090b1df](https://gitlab.com/hoppr/modules/security-commons/commit/090b1df467e9afa1f1bcec26818884864c149c61))

## [0.0.91](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.90...v0.0.91) (2024-06-05)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([c4b1e80](https://gitlab.com/hoppr/modules/security-commons/commit/c4b1e803504ea381e52369a0b942b63353f54f30))

## [0.0.90](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.89...v0.0.90) (2024-05-29)


### Bug Fixes

* **deps:** update dependency requests to v2.32.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2872b32](https://gitlab.com/hoppr/modules/security-commons/commit/2872b329b3f4bf66a4b66cba894439cb538ab745))

## [0.0.89](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.88...v0.0.89) (2024-05-21)


### Bug Fixes

* **deps:** update dependency requests to v2.32.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1bd33d5](https://gitlab.com/hoppr/modules/security-commons/commit/1bd33d595fbee0aabb1ce01a5918db863569784c))

## [0.0.88](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.87...v0.0.88) (2024-05-20)


### Bug Fixes

* **deps:** update dependency requests to v2.32.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([889be25](https://gitlab.com/hoppr/modules/security-commons/commit/889be25c303c8de8396d69d9a3605d7d143fb7ce))

## [0.0.87](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.86...v0.0.87) (2024-05-20)


### Bug Fixes

* **deps:** update dependency requests to v2.32.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([606b375](https://gitlab.com/hoppr/modules/security-commons/commit/606b3752403206c5ebb39fa72ce295f01055ae89))

## [0.0.86](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.85...v0.0.86) (2024-05-06)


### Bug Fixes

* **deps:** update dependency jinja2 to v3.1.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([75e27cd](https://gitlab.com/hoppr/modules/security-commons/commit/75e27cdc924c4cbd090c96fd4c872c134b6bd405))

## [0.0.85](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.84...v0.0.85) (2024-04-09)


### Bug Fixes

* **deps:** update dependency typer to v0.12.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([018c5be](https://gitlab.com/hoppr/modules/security-commons/commit/018c5be60c0bfff169402ebebc14f859014f557b))

## [0.0.84](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.83...v0.0.84) (2024-04-08)


### Bug Fixes

* **deps:** update dependency typer to v0.12.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([87c1e3c](https://gitlab.com/hoppr/modules/security-commons/commit/87c1e3c163a59263349005d82385782f04a56e59))

## [0.0.83](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.82...v0.0.83) (2024-04-05)


### Bug Fixes

* **deps:** update dependency typer to v0.12.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([cd7c87a](https://gitlab.com/hoppr/modules/security-commons/commit/cd7c87a65349830b6eb3925827a4c0f1a03f1692))

## [0.0.82](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.81...v0.0.82) (2024-03-30)


### Bug Fixes

* **deps:** update dependency typer to v0.12.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e339f2d](https://gitlab.com/hoppr/modules/security-commons/commit/e339f2d8f489f7c99cbe7bbb4785cd7de521886a))

## [0.0.81](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.80...v0.0.81) (2024-03-28)


### Bug Fixes

* **deps:** update dependency typer to v0.11.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e5e2afa](https://gitlab.com/hoppr/modules/security-commons/commit/e5e2afacd91d30a56f22054a76a0ae04f55e602c))

## [0.0.80](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.79...v0.0.80) (2024-03-27)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2474634](https://gitlab.com/hoppr/modules/security-commons/commit/2474634769849112c4b120e72d88dab4ac0365c3))

## [0.0.79](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.78...v0.0.79) (2024-03-27)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1893264](https://gitlab.com/hoppr/modules/security-commons/commit/18932649ecf113bbb7f4a39826c11c7fae847695))

## [0.0.78](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.77...v0.0.78) (2024-03-26)


### Bug Fixes

* **deps:** update dependency typer to v0.11.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e1a4935](https://gitlab.com/hoppr/modules/security-commons/commit/e1a4935d804c7b6f6d0e9c8d1a1439072a7ff298))

## [0.0.77](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.76...v0.0.77) (2024-03-23)


### Bug Fixes

* **deps:** update dependency typer to v0.10.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([838669a](https://gitlab.com/hoppr/modules/security-commons/commit/838669a4afb732ba766e347417add1ef8c6615b2))

## [0.0.76](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.75...v0.0.76) (2024-03-23)


### Bug Fixes

* **deps:** update dependency typer to v0.9.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d5c5618](https://gitlab.com/hoppr/modules/security-commons/commit/d5c5618facfd53f533bb16c39f944819dcaede0f))

## [0.0.75](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.74...v0.0.75) (2024-03-12)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.15.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([162930c](https://gitlab.com/hoppr/modules/security-commons/commit/162930cb44fab0623ce3b129fcddac719567ede6))

## [0.0.74](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.73...v0.0.74) (2024-02-29)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.14.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([88a8d0a](https://gitlab.com/hoppr/modules/security-commons/commit/88a8d0a1d9711b634923059d26455298d0584b6f))

## [0.0.73](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.72...v0.0.73) (2024-01-11)


### Bug Fixes

* **deps:** update dependency jinja2 to v3.1.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([085cde6](https://gitlab.com/hoppr/modules/security-commons/commit/085cde663d5cb65779ec3e27901935696fd6c49a))

## [0.0.72](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.71...v0.0.72) (2024-01-08)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.13.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ca50993](https://gitlab.com/hoppr/modules/security-commons/commit/ca509937fd4dffbca684ec5d76059a7c7b681a2a))

## [0.0.71](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.70...v0.0.71) (2024-01-05)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([71cee92](https://gitlab.com/hoppr/modules/security-commons/commit/71cee925616d4ce6874d89a9048e88763a4cedb3))

## [0.0.70](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.69...v0.0.70) (2024-01-05)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.13.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([392669d](https://gitlab.com/hoppr/modules/security-commons/commit/392669d8388e3b8356d1292f1a0898faea334f5c))

## [0.0.69](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.68...v0.0.69) (2024-01-04)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.13.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1f8ae1d](https://gitlab.com/hoppr/modules/security-commons/commit/1f8ae1d1dab77edc5ec8477d78249f3f00d5f15b))

## [0.0.68](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.67...v0.0.68) (2023-12-19)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.4.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5322b56](https://gitlab.com/hoppr/modules/security-commons/commit/5322b564ac7be6d8990bf163f2858e6e022b0afa))

## [0.0.67](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.66...v0.0.67) (2023-12-18)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.3.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([36fa89d](https://gitlab.com/hoppr/modules/security-commons/commit/36fa89df0d5998c6c592e6fbe6ff97c8fdbbfa0c))

## [0.0.66](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.65...v0.0.66) (2023-12-13)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.13.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f315c93](https://gitlab.com/hoppr/modules/security-commons/commit/f315c93f3e23175f7364f4d8cda6484716822d3a))

## [0.0.65](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.64...v0.0.65) (2023-12-11)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.13.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([aa509ca](https://gitlab.com/hoppr/modules/security-commons/commit/aa509ca0616bf50e976c6679e01997a9b19b805e))

## [0.0.64](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.63...v0.0.64) (2023-12-08)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.12.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2cc2fbf](https://gitlab.com/hoppr/modules/security-commons/commit/2cc2fbf539f7c27a2c38b594e40aad3db84830ad))

## [0.0.63](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.62...v0.0.63) (2023-12-08)


### Bug Fixes

* **deps:** update dependency packageurl-python to v0.11.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4c3e38f](https://gitlab.com/hoppr/modules/security-commons/commit/4c3e38f8749ee1cd008fcde689d86693c9837bd4))

## [0.0.62](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.61...v0.0.62) (2023-12-04)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.3.6 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0c1ec5e](https://gitlab.com/hoppr/modules/security-commons/commit/0c1ec5e002baba0e98c7175283ad53a984f657d9))

## [0.0.61](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.60...v0.0.61) (2023-10-26)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.3.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5134775](https://gitlab.com/hoppr/modules/security-commons/commit/51347758b5134b931f5be3439ce04c1068666efd))

## [0.0.60](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.59...v0.0.60) (2023-10-24)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.3.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6e5ce25](https://gitlab.com/hoppr/modules/security-commons/commit/6e5ce25f24c3500b3ae2ad280156c272aad99cad))

## [0.0.59](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.58...v0.0.59) (2023-10-11)


### Bug Fixes

* create output_path if non-existent ([8b0be17](https://gitlab.com/hoppr/modules/security-commons/commit/8b0be17146824c39fba5d0892b7bf2b13793a493))

## [0.0.58](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.57...v0.0.58) (2023-09-27)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.5.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6687a23](https://gitlab.com/hoppr/modules/security-commons/commit/6687a23be10a701687f439be4e8ba125172bb47e))

## [0.0.57](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.56...v0.0.57) (2023-09-18)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.5.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4d49702](https://gitlab.com/hoppr/modules/security-commons/commit/4d49702eb6c9a3d18dcb547ce0f795bc614a1f1a))

## [0.0.56](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.55...v0.0.56) (2023-09-12)


### Bug Fixes

* fix errors in reporting.py ([4a8b348](https://gitlab.com/hoppr/modules/security-commons/commit/4a8b3481d0fb9a181eb6b697271bdca411993b12))
* remove unnecessary str() calls ([bb1de6f](https://gitlab.com/hoppr/modules/security-commons/commit/bb1de6f0a5dfc78f8657337f690e471a7fe9a00b))

## [0.0.55](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.54...v0.0.55) (2023-08-15)


### Bug Fixes

* fix unexpected type bug caused by base model config setting ([19626d8](https://gitlab.com/hoppr/modules/security-commons/commit/19626d8b78215d0c3bc7eb638f36176432ca9ca2))

## [0.0.54](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.53...v0.0.54) (2023-08-10)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.5.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f2aead4](https://gitlab.com/hoppr/modules/security-commons/commit/f2aead41fd6a3408b79a83f1b1046f8e75607b3a))

## [0.0.53](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.52...v0.0.53) (2023-08-09)


### Bug Fixes

* bumped versions in poetry lock file ([2f9cd67](https://gitlab.com/hoppr/modules/security-commons/commit/2f9cd67c967cd25bf427d9aa55c9360519707575))
* corrected versions in poetry lock file ([c3e6f1a](https://gitlab.com/hoppr/modules/security-commons/commit/c3e6f1a7ed20d0d3ef31bf0d2aacc19402bfbf3a))

## [0.0.52](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.51...v0.0.52) (2023-07-24)


### Bug Fixes

* revert versions for compatibility with hoppr and hopprcop ([1edfe4e](https://gitlab.com/hoppr/modules/security-commons/commit/1edfe4e09eaf934f5c88449417128a5a2316da6b))

## [0.0.51](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.50...v0.0.51) (2023-07-21)


### Bug Fixes

* reverting models update ([82af7fc](https://gitlab.com/hoppr/modules/security-commons/commit/82af7fcef4515bb1f2c9d3d878d9b1ebdac3a8fa))

## [0.0.50](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.49...v0.0.50) (2023-07-20)


### Bug Fixes

* added offline support and checks ([c2e5c85](https://gitlab.com/hoppr/modules/security-commons/commit/c2e5c856f24e204e93d2c9dceea23df1ec50c738))

## [0.0.49](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.48...v0.0.49) (2023-07-13)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.3.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([a8398c7](https://gitlab.com/hoppr/modules/security-commons/commit/a8398c75062ea5616ac95665af590b53d9438b32))

## [0.0.48](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.47...v0.0.48) (2023-06-26)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to ^0.5.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([de23e89](https://gitlab.com/hoppr/modules/security-commons/commit/de23e89df30aa54ae6004cd359a4e1d66230f0b1))

## [0.0.47](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.46...v0.0.47) (2023-06-19)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.4.25 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([3b48b80](https://gitlab.com/hoppr/modules/security-commons/commit/3b48b80905aa73572cc65cbe889223e4ab97583f))

## [0.0.46](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.45...v0.0.46) (2023-05-22)


### Bug Fixes

* **deps:** update dependency requests to v2.31.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0695888](https://gitlab.com/hoppr/modules/security-commons/commit/0695888eb2258f6c895c37257599480f60349da6))

## [0.0.45](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.44...v0.0.45) (2023-05-16)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.4.24 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([21c755c](https://gitlab.com/hoppr/modules/security-commons/commit/21c755c62d4abaec9521e91f9f50ecc6f2075566))

## [0.0.44](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.43...v0.0.44) (2023-05-15)


### Bug Fixes

* removing unused npm configs ([2fd9620](https://gitlab.com/hoppr/modules/security-commons/commit/2fd962076d408c80f5c03274bff338db658a0058))

## [0.0.43](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.42...v0.0.43) (2023-05-10)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.13.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([11e6243](https://gitlab.com/hoppr/modules/security-commons/commit/11e62432a2d38b47726ed09120b8b94da4c5c4a2))

## [0.0.42](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.41...v0.0.42) (2023-05-04)


### Bug Fixes

* **deps:** update dependency requests to v2.30.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5afe265](https://gitlab.com/hoppr/modules/security-commons/commit/5afe26503fa25b1a96dd702cf1fce9e9a2262976))

## [0.0.41](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.40...v0.0.41) (2023-05-03)


### Bug Fixes

* **deps:** update dependency lunr-languages to v1.12.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9553cb5](https://gitlab.com/hoppr/modules/security-commons/commit/9553cb5de763768160b267a7d6ca8cf2cf4f57ce))

## [0.0.40](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.39...v0.0.40) (2023-05-02)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.12.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([10d6b0b](https://gitlab.com/hoppr/modules/security-commons/commit/10d6b0b83909be34c6d0573f54dd983b41aa2879))

## [0.0.39](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.38...v0.0.39) (2023-05-02)


### Bug Fixes

* **deps:** update dependency typer to ^0.9.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([02f5b6e](https://gitlab.com/hoppr/modules/security-commons/commit/02f5b6e37fb53029e07d90163489a8b6ea39a945))

## [0.0.38](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.37...v0.0.38) (2023-05-01)


### Bug Fixes

* **deps:** update dependency typer to ^0.8.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e7c523e](https://gitlab.com/hoppr/modules/security-commons/commit/e7c523ea9a940b5020e5e3c49d342fd7a6e1300f))

## [0.0.37](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.36...v0.0.37) (2023-04-29)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.4.23 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ed05d98](https://gitlab.com/hoppr/modules/security-commons/commit/ed05d9886f6fa604416617758cf5d79af6257261))

## [0.0.36](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.35...v0.0.36) (2023-04-27)


### Bug Fixes

* **deps:** update dependency rxjs to v7.8.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([159c067](https://gitlab.com/hoppr/modules/security-commons/commit/159c06778c899c06ac0d5b9a52b8584068333708))

## [0.0.35](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.34...v0.0.35) (2023-04-27)


### Bug Fixes

* **deps:** update dependency rich to v13.3.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([fb06da0](https://gitlab.com/hoppr/modules/security-commons/commit/fb06da0af412ac3e0474fbf9d39953f66e53621e))

## [0.0.34](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.33...v0.0.34) (2023-04-26)


### Bug Fixes

* **deps:** update dependency lunr-languages to v1.11.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e77a4d1](https://gitlab.com/hoppr/modules/security-commons/commit/e77a4d11c97114e5e100c60d2d518390ea87bbcd))

## [0.0.33](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.32...v0.0.33) (2023-04-26)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.3.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0f8d47b](https://gitlab.com/hoppr/modules/security-commons/commit/0f8d47b953a98277e6dbbf043901927cd6afabc4))

## [0.0.32](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.31...v0.0.32) (2023-04-25)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.12.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ddb74c5](https://gitlab.com/hoppr/modules/security-commons/commit/ddb74c5b8b5a44461d73dfd8176d33fd11e932aa))

## [0.0.31](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.30...v0.0.31) (2023-04-17)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.12.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([7618d42](https://gitlab.com/hoppr/modules/security-commons/commit/7618d427b615426bccc001ecbdad8e85aabbada8))

## [0.0.30](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.29...v0.0.30) (2023-04-12)


### Bug Fixes

* **deps:** update dependency rich to v13.3.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([7531476](https://gitlab.com/hoppr/modules/security-commons/commit/75314766caa32b2d3546dcd40f6f9b31a2e7fa79))

## [0.0.29](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.28...v0.0.29) (2023-04-11)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.12.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([62c833f](https://gitlab.com/hoppr/modules/security-commons/commit/62c833f6d6bc1451e475d23aec6136c9ba80be11))

## [0.0.28](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.27...v0.0.28) (2023-04-07)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.4.22 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0be8807](https://gitlab.com/hoppr/modules/security-commons/commit/0be880733ec225018303ed71e2a18e6c2765e39e))

## [0.0.27](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.26...v0.0.27) (2023-04-07)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.4.21 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b5ccd16](https://gitlab.com/hoppr/modules/security-commons/commit/b5ccd169f4cf3c70dc5add666c74d9898c853ac7))

## [0.0.26](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.25...v0.0.26) (2023-04-05)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.11.16 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4f52b86](https://gitlab.com/hoppr/modules/security-commons/commit/4f52b862a27b1f56b6b9c33510b00323e2364f80))

## [0.0.25](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.24...v0.0.25) (2023-04-01)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.4.20 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e8b52da](https://gitlab.com/hoppr/modules/security-commons/commit/e8b52dadc22aa4f32719267edc2a16e83710d479))

## [0.0.24](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.23...v0.0.24) (2023-03-30)


### Bug Fixes

* **deps:** update dependency tailwindcss to v3.3.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([852efd1](https://gitlab.com/hoppr/modules/security-commons/commit/852efd18e193dd221ce09658ac92a68b2702ba15))

## [0.0.23](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.22...v0.0.23) (2023-03-30)


### Bug Fixes

* **deps:** update dependency packageurl-python to ^0.11.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ec5f054](https://gitlab.com/hoppr/modules/security-commons/commit/ec5f05418c2c266111247ca5a4d8de7646687a64))
* **deps:** update dependency tailwindcss to v3.3.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([85edc03](https://gitlab.com/hoppr/modules/security-commons/commit/85edc03a44d50199a9ba216de747831d3bc6c5d8))

## [0.0.22](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.21...v0.0.22) (2023-03-30)


### Bug Fixes

* **deps:** update dependency rich to v13.3.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([a306dd1](https://gitlab.com/hoppr/modules/security-commons/commit/a306dd1336a572b5022fe5cf604fc75c1464e13f))

## [0.0.21](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.20...v0.0.21) (2023-03-29)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.11.15 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8c9899f](https://gitlab.com/hoppr/modules/security-commons/commit/8c9899f259d9a1398b95cd160bc75aae7b8459a2))

## [0.0.20](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.19...v0.0.20) (2023-03-21)


### Bug Fixes

* **deps:** update dependency @mui/material to v5.11.14 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b2a988f](https://gitlab.com/hoppr/modules/security-commons/commit/b2a988f8a7e625f9ddf257c450339f4cc35582b2))

## [0.0.19](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.18...v0.0.19) (2023-03-15)


### Bug Fixes

* **deps:** replace dependency @material-ui/core with @mui/material ^5.0.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([3f667df](https://gitlab.com/hoppr/modules/security-commons/commit/3f667df3b1e7da81eb4a5c2ce87d61603cb78ab4))

## [0.0.18](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.17...v0.0.18) (2023-03-14)


### Bug Fixes

* **deps:** update dependency rxjs to v7.8.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9eac097](https://gitlab.com/hoppr/modules/security-commons/commit/9eac097a428713509aef59b126009cf74f6661f9))
* **deps:** update dependency tailwindcss to v3.2.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([c7c2c14](https://gitlab.com/hoppr/modules/security-commons/commit/c7c2c142ff337c00906e590b85654f26c5464a76))

## [0.0.17](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.16...v0.0.17) (2023-03-14)


### Bug Fixes

* **deps:** update dependency lunr-languages to v1.10.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([327332a](https://gitlab.com/hoppr/modules/security-commons/commit/327332aa1a4222de9f42b765aa7de528fe0cc9f6))

## [0.0.16](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.15...v0.0.16) (2023-03-14)


### Bug Fixes

* **deps:** update dependency rich to v13.3.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5563406](https://gitlab.com/hoppr/modules/security-commons/commit/5563406cc26f1e918cb52e4c0c86515ee71afc70))

## [0.0.15](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.14...v0.0.15) (2023-03-14)


### Bug Fixes

* **deps:** update dependency iframe-worker to v1.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9d0b8b7](https://gitlab.com/hoppr/modules/security-commons/commit/9d0b8b73aedf959317972f625c53e5a6c78013f1))

## [0.0.14](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.13...v0.0.14) (2023-03-14)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to v0.4.10 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ab669d2](https://gitlab.com/hoppr/modules/security-commons/commit/ab669d2478ac4e11251cac9047b5a48ec13d5411))

## [0.0.13](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.12...v0.0.13) (2023-02-25)


### Bug Fixes

* exposing previously private utility function to build a dictionary from purls ([4750db8](https://gitlab.com/hoppr/modules/security-commons/commit/4750db8372852b4ec8d8788791b026ac45e6a329))

## [0.0.12](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.11...v0.0.12) (2023-02-21)


### Bug Fixes

* Update dependencies ([2eefa76](https://gitlab.com/hoppr/modules/security-commons/commit/2eefa765e1bf93d9f8c745c21f9c8c99ca0a0b82))

## [0.0.11](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.10...v0.0.11) (2023-02-06)


### Bug Fixes

* **deps:** update dependency rich to v13 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([a61092d](https://gitlab.com/hoppr/modules/security-commons/commit/a61092d62b3b3b46931de789b5c3505ca1f02c9b))

## [0.0.10](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.9...v0.0.10) (2023-02-06)


### Bug Fixes

* **deps:** update dependency hoppr-cyclonedx-models to ^0.4.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([94c30fd](https://gitlab.com/hoppr/modules/security-commons/commit/94c30fd5c67e27f1df2000ce1b6c1efc1ae4dd13))

## [0.0.9](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.8...v0.0.9) (2023-02-06)


### Bug Fixes

* added vex linking ([5f88286](https://gitlab.com/hoppr/modules/security-commons/commit/5f8828660039d860b4d04ea86b37b8ab4f9b2d96))

## [0.0.8](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.7...v0.0.8) (2023-01-25)


### Bug Fixes

* updated cyclonedx models ([539f221](https://gitlab.com/hoppr/modules/security-commons/commit/539f221a87daf3d8a6741da8fa1ad747fdeff0de))

## [0.0.7](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.6...v0.0.7) (2023-01-24)


### Bug Fixes

* updated gitlab reporting ([813e3bd](https://gitlab.com/hoppr/modules/security-commons/commit/813e3bdcdf47578a5ee4bc966479f02b32b32745))

## [0.0.6](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.5...v0.0.6) (2023-01-09)


### Bug Fixes

* set exclude_set to true ([7c10a4b](https://gitlab.com/hoppr/modules/security-commons/commit/7c10a4bb13c2431ad7f4894bb9dbce41c3c433f9))

## [0.0.5](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.4...v0.0.5) (2022-12-31)


### Bug Fixes

* **deps:** update dependency unfetch to v5 ([80f4d33](https://gitlab.com/hoppr/modules/security-commons/commit/80f4d334454a2bc6350e8f499ddc97f91c420da0))

## [0.0.4](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.3...v0.0.4) (2022-12-15)


### Bug Fixes

* **deps:** update dependency iframe-worker to v1 ([d92a5b1](https://gitlab.com/hoppr/modules/security-commons/commit/d92a5b14c8881d28e903520ae0ad04665fdd627e))

## [0.0.3](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.2...v0.0.3) (2022-12-12)


### Bug Fixes

* **deps:** update dependency tabulate to ^0.9.0 ([ec3443d](https://gitlab.com/hoppr/modules/security-commons/commit/ec3443dcb8d39feb0dbef3316908fb1951c6c1ef))

## [0.0.2](https://gitlab.com/hoppr/modules/security-commons/compare/v0.0.1...v0.0.2) (2022-12-12)


### Bug Fixes

* Correct tags and versioning ([4a07e84](https://gitlab.com/hoppr/modules/security-commons/commit/4a07e8420768e5f2bad3ea00a52615abba5ae2fe))
